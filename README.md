# Esp8266 + DHT22

Código feito para o projeto GreenHouse Keeper. Faz a leitura de temperatura do 
módulo DHT22 e faz o envio para a api.

## Wiki

[https://gitlab.com/greenhouseKeeper/api/wikis/pages](https://gitlab.com/greenhouseKeeper/api/wikis/pages)

### Ferramentas

*  Esp8266 NodeMcu v3
*  DHT22

### Linguagens

*  C++

### Bibliotecas

*  DHT.h
*  ESP8266WiFi.h
*  ESP8266HTTPClient.h
*  WiFiClient.h
*  Adafruit_Sensor.h

### Programas utilizados

*  Arduino IDE 1.8.5 => [https://www.arduino.cc/en/main/software](url)

