#include "DHT.h"

#define DHTPIN 0

#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

DHT dht(DHTPIN, DHTTYPE);

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

const char* ssid     = "CASA";
const char* password = "casa7564";

const char* serverName = "https://frozen-bayou-51290.herokuapp.com/api/temperature";

String apiKeyValue = "base64:NaRW9YX/ajECTR9Fei74zC/zUHm99d3dbtmRprW1gzc=";

#include <Wire.h>
#include <Adafruit_Sensor.h>
// #include <Adafruit_BME280.h>

// String sensorName = "BME280";
String sensorLocation = "Office";

#define SEALEVELPRESSURE_HPA (1013.25)

// Adafruit_BME280 bme;

void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  dht.begin();
}

void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    float h = dht.readHumidity();

    float t = dht.readTemperature();

    float f = dht.readTemperature(true);

    if (isnan(h) || isnan(t) || isnan(f)) {
      Serial.println(F("Failed to read from DHT sensor!"));
      delay(1000);
      return;
    }

    float hif = dht.computeHeatIndex(f, h);

    float hic = dht.computeHeatIndex(t, h, false);

    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.print(F("°C "));
    Serial.print(f);
    Serial.print(F("°F  Heat index: "));
    Serial.print(hic);
    Serial.print(F("°C "));
    Serial.print(hif);
    Serial.println(F("°F"));

    HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin("http://frozen-bayou-51290.herokuapp.com/api/temperature?celsius_temperature=" + String(t));      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.POST("");   //Send the request
   String payload = http.getString();                  //Get the response payload
 
   Serial.println(httpCode);   //Print HTTP return code
   Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
  } else {
    Serial.println("WiFi Disconnected");
  }

  delay(5000);
}
